import numpy as np
import itertools
import copy
import pandas as pd
import sys
from sklearn.externals import joblib
from sklearn.linear_model import LinearRegression
import statsmodels.formula.api as smf
import statsmodels.api as sm
import statsmodels

FEATURES = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration"]
FEATURESALL = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration", "FFPType"]
FEATURES_FFP = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration","walk", "trot", "gallop","dynamic walk", "walk2", "walk2LongSwing", "walkLongSwing" ]
FEATURESALL_WTS = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration", "FFPType", "happier_wt", "sadder_wt", "angrier_wt", "more-afraid_wt"]
FFP_LABELS = ["walk", "trot", "gallop","dynamic walk", "walk2", "walk2LongSwing", "walkLongSwing"]

EMOSCORES = ["happier_mu", "sadder_mu", "angrier_mu", "more-afraid_mu", "more-surprised_mu", "more-disgusted_mu"]
EMOCONFIDENCE_INV = ["happier_sigma", "sadder_sigma", "angrier_sigma", "more-afraid_sigma","more-surprised_sigma", "more-disgusted_sigma"]
min_confidence = 25/3 # true skill base confidence: http://trueskill.org/
WTS_LABEL = ["happier_wt", "sadder_wt", "angrier_wt", "more-afraid_wt","more-surprised_wt", "more-disgusted_wt"]

model_dir_name = "/Users/rutad/Documents/bitbucket/pythonTF/LRBaseline/"

# load dataset
X_train_all_wts = pd.read_csv(model_dir_name+"XTrainLR.csv", delimiter=',', lineterminator='\n');
X_test_all_wts = pd.read_csv(model_dir_name+"XTestLR.csv", delimiter=',', lineterminator='\n');
motionData = pd.read_csv('C:\\Users\\rutad\\Documents\\bitbucket\\code\\walkingDataset\\motionData.csv', delimiter=',', lineterminator='\n');
motionData.rename(columns={"FFPType\r": "FFPType"},inplace=True)
X_train_all_wts.rename(columns={"walkLongSwing\r": "walkLongSwing"},inplace=True)
X_test_all_wts.rename(columns={"walkLongSwing\r": "walkLongSwing"},inplace=True)

regressor_happy = joblib.load(model_dir_name + "SLRTrained_"+EMOSCORES[0]+".pkl")
regressor_sad = joblib.load(model_dir_name + "SLRTrained_"+EMOSCORES[1]+".pkl")
regressor_angry = joblib.load(model_dir_name + "SLRTrained_"+EMOSCORES[2]+".pkl")
regressor_scared = joblib.load(model_dir_name + "SLRTrained_"+EMOSCORES[3]+".pkl")
regressor_surprised = joblib.load(model_dir_name + "SLRTrained_"+EMOSCORES[4]+".pkl")
regressor_disgusted = joblib.load(model_dir_name + "SLRTrained_"+EMOSCORES[5]+".pkl")

happy_model = np.array([ 22.70921329,  -0.49739174,   0.43501176,   0.18048968,  -0.49100797,
  -0.12632153,   0.20959529,  -0.1315793,    0.67025964,   1.12883121,
  -2.71626753,  1.55034779,   7.98968029,   4.4749828,    2.8306612,
   0.96046089,   1.60507902,   3.29800131])
sad_model = np.array([ 22.45333954,   0.5013537,    0.07973248,  -0.47844291,   0.36989913,
   0.59047851,   0.16240239,   0.42790842,  -0.72975969,  -1.39209473,
   3.46273166,   4.35588575,   1.09612481,  2.53414826,   2.95132042,
   4.7819451,    3.70821217,   3.02570303])
angry_model = np.array([ 22.5955013,    1.23163582,  -0.11039642,   0.37738197,  -0.79091749,
  -0.07548601,   0.18598456,   0.29902765,   0.4924015,    0.99202661,
  -1.65724732,   3.07959956,   1.27466083,   3.05603761,   4.60662065,
   3.21455533,   3.14919219,   4.21483513])
scared_model = np.array([ 22.63629953,  -0.15748383,  -0.57033733,  -0.35477563,   0.2879764,
   0.18719958,   0.4353748,   -0.31640269,  -0.45824592,  -1.5131049,
   1.96807204,   4.50706602,   1.69902379,   3.62729368,   1.40786332,
   5.01069032,   3.55160227,   2.83276014])
disgusted_model = np.array([ 22.69037987,   0.81893473,  -0.50944466,   0.10672881,
         0.34574323,   0.66038011,   0.02740522,   0.08844757,
         0.03082846,  -1.23411577,   1.85656852,   4.38159771,
         2.39871267,   3.60124734,   2.81902811,   3.82549639,
         2.81243276,   2.85186491])
surprised_model = np.array([ 22.45991106,   0.12390727,  -0.06419796,  -0.42763204,
         0.58054639,   0.10996926,   0.02537607,  -0.39313884,
         0.66622098,  -0.2193388 ,  -0.82520438,   2.94632798,
         5.23533682,   3.58585564,   1.33694337,   2.19000575,
         3.62013665,   3.54530485])

#print("DONE!")