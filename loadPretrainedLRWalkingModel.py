import numpy as np
import itertools
import copy
import pandas as pd
import sys
from sklearn.externals import joblib
from sklearn.linear_model import LinearRegression

FEATURES = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration"]
FEATURESALL = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration", "FFPType"]
FEATURES_FFP = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration","walk", "trot", "gallop","dynamic walk", "walk2", "walk2LongSwing", "walkLongSwing" ]
FEATURESALL_WTS = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration", "FFPType", "happier_wt", "sadder_wt", "angrier_wt", "more-afraid_wt"]
FFP_LABELS = ["walk", "trot", "gallop","dynamic walk", "walk2", "walk2LongSwing", "walkLongSwing"]

EMOSCORES = ["happier_mu", "sadder_mu", "angrier_mu", "more-afraid_mu"]
EMOCONFIDENCE_INV = ["happier_sigma", "sadder_sigma", "angrier_sigma", "more-afraid_sigma"]
min_confidence = 25/3 # true skill base confidence: http://trueskill.org/
WTS_LABEL = ["happier_wt", "sadder_wt", "angrier_wt", "more-afraid_wt"]

model_dir_name = "/Users/rutad/Documents/bitbucket/pythonTF/LRBaseline/"

# load dataset
X_train_all_wts = pd.read_csv(model_dir_name+"XTrainLR.csv", delimiter=',', lineterminator='\n');
X_test_all_wts = pd.read_csv(model_dir_name+"XTestLR.csv", delimiter=',', lineterminator='\n');
motionData = pd.read_csv('C:\\Users\\rutad\\Documents\\bitbucket\\code\\walkingDataset\\motionData.csv', delimiter=',', lineterminator='\n');
motionData.rename(columns={"FFPType\r": "FFPType"},inplace=True)
X_train_all_wts.rename(columns={"walkLongSwing\r": "walkLongSwing"},inplace=True)
X_test_all_wts.rename(columns={"walkLongSwing\r": "walkLongSwing"},inplace=True)

regressor_happy = joblib.load(model_dir_name + "LRTrained_"+EMOSCORES[0]+".pkl")
regressor_sad = joblib.load(model_dir_name + "LRTrained_"+EMOSCORES[1]+".pkl")
regressor_angry = joblib.load(model_dir_name + "LRTrained_"+EMOSCORES[2]+".pkl")
regressor_scared = joblib.load(model_dir_name + "LRTrained_"+EMOSCORES[3]+".pkl")

#print("DONE!")