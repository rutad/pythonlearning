## load NN model
from pathlib import Path
import os
import timeit
from loadPretrainedBestWalkingModel import *

save_name_dir ="../walkingDataset/tmp/"
handshake = Path(save_name_dir + "paramsReady.txt");
oldparams = [];
global computeDone
computeDone = False

while True:
	if handshake.exists():

		PARAM_ID = int(sys.argv[1]) 
		#print("slide id")
		#print(PARAM_ID)

		## read params from cpp
		fname = save_name_dir + "params.txt" 
		with open(fname) as f:
		    params = f.readlines()
		params =  [x.strip('\n') for x in params] 

		fname2 = save_name_dir + "plotsReady.txt"
		with open(fname2) as f2:
		    params2 = f2.readlines()
		params2 =  [x.strip('\n') for x in params2] 
		#print(params2)

		if not params2:
			computeDone = False
		#else:
			#computeDone = True
		#print(computeDone)

		if(computeDone == False):
			if(PARAM_ID not in params2):
			#if(params!=oldparams):
				#oldparams = params;
				#print(len(params))

				## COMPUTE CURVES
				test_feature_vals = pd.DataFrame(index=[0], columns=FEATURESALL)
				test_feature_vals = test_feature_vals.fillna(0) # with 0s

				PARAM_NAME = FEATURESALL[PARAM_ID];
				#print(PARAM_NAME)

				num_eval = 50;
				param_stepSize = (max(X_train_all_wts[PARAM_NAME].max(),X_test_all_wts[PARAM_NAME].max()) - min(X_train_all_wts[PARAM_NAME].min(),X_test_all_wts[PARAM_NAME].min()))/float(num_eval);
				#print(param_stepSize)
				param_range = np.arange(min(X_train_all_wts[PARAM_NAME].min(),X_test_all_wts[PARAM_NAME].min()),
				max(X_train_all_wts[PARAM_NAME].max(),X_test_all_wts[PARAM_NAME].max()) + param_stepSize,param_stepSize)
				if(PARAM_NAME == "FFPType"):
					param_range = np.array([0,1,2,3,4,5,6]);
				#print(param_range)

				count = 0
				if(len(params)== len(FEATURESALL) + 1):
					for f in FEATURESALL:
						if(f!="FFPType"):
							test_feature_vals[f] = (float(params[count]) - motionData[f].mean())/motionData[f].std();
						else:
							test_feature_vals[f] = float(params[count])
						count = count + 1

				wt_vals = np.array([1,1,1]); # wts
				#actual sample
				original_vals = np.copy(test_feature_vals.values)
				original_vals = np.append(original_vals, wt_vals)
				original_vals =  original_vals.reshape(1,len(FEATURESALL_WTS))
				X_param_original_df = pd.DataFrame(original_vals, columns = FEATURESALL_WTS)
				X_param_original_df["FFPType"]=X_param_original_df["FFPType"].astype(np.int64) 

				for s in param_range:
					if(s==min(X_train_all_wts[PARAM_NAME].min(),X_test_all_wts[PARAM_NAME].min())): 
						test_vals = np.copy(test_feature_vals.values)
						test_vals = np.append(test_vals, wt_vals)
						test_vals[PARAM_ID] = s;
						X_param_test = test_vals
					else:
						test_vals2 = np.copy(test_feature_vals.values)
						test_vals2 = np.append(test_vals2, wt_vals)
						test_vals2[PARAM_ID] = s;
						X_param_test = np.append(X_param_test, test_vals2,axis=0)
				X_param_test = X_param_test.reshape(param_range.size,len(FEATURESALL_WTS))
				X_param_test_df = pd.DataFrame(X_param_test, columns = FEATURESALL_WTS)
				X_param_test_df["FFPType"]=X_param_test_df["FFPType"].astype(np.int64) 

				X_param_test_concat_df = pd.concat([X_param_test_df, X_param_original_df])

				start_time = timeit.default_timer()
				#print(start_time)

				# choose different regressor options based on emotion type..
				if(int(params[11]) == 0):
					#with tf.device("/cpu:0"):
					predictions_param_range = regressorHappy.predict_scores(input_fn=get_input_fn_predict(X_param_test_concat_df, [], [], num_epochs=1, shuffle=False))
					#with tf.device("/cpu:1"):
					#predictions_original = regressorHappy.predict_scores(input_fn=get_input_fn_predict(X_param_original_df, [], [], num_epochs=1, shuffle=False))
					emoFilenameType = "happy_"
				elif(int(params[11]) == 1):
					predictions_param_range = regressorSad.predict_scores(input_fn=get_input_fn_predict(X_param_test_concat_df, [], [], num_epochs=1, shuffle=False))
					#predictions_original = regressorSad.predict_scores(input_fn=get_input_fn_predict(X_param_original_df, [], [], num_epochs=1, shuffle=False))
					emoFilenameType = "sad_"
				y_predicted_param_all = list(itertools.islice(predictions_param_range,X_param_test_concat_df["FFPType"].size))
				num_pred = len(y_predicted_param_all)
				y_predicted_param = y_predicted_param_all[:num_pred-1]
				#y_predicted_original = list(itertools.islice(predictions_original,X_param_original_df["FFPType"].size))
				y_predicted_original = y_predicted_param_all[-1]
				#print(y_predicted_original)

				elapsed = timeit.default_timer() - start_time
				print(elapsed)

				# save plot data in file..
				PARAM_FILENAME = copy.copy(PARAM_NAME);
				if(PARAM_NAME == "bodyAngleZ"):
					PARAM_FILENAME = "body angle";
				elif(PARAM_NAME == "swingFtHeight"):
					PARAM_FILENAME = "foot height";
				elif(PARAM_NAME == "gaitDuration"):
					PARAM_FILENAME = "gait time";
				elif(PARAM_NAME == "frontHipsAngle"):
					PARAM_FILENAME = "front hips";
				elif(PARAM_NAME == "rearHipsAngle"):
					PARAM_FILENAME = "rear hips";
				elif(PARAM_NAME == "frontKneesAngle"):
					PARAM_FILENAME = "front knees";
				elif(PARAM_NAME == "rearKneesAngle"):
					PARAM_FILENAME = "rear knees";
				elif(PARAM_NAME == "frontAnkle"):
					PARAM_FILENAME = "front ankles";
				elif(PARAM_NAME == "rearAnkle"):
					PARAM_FILENAME = "rear ankles";
				elif(PARAM_NAME == "FFPType"):
					PARAM_FILENAME = "gait pattern";
				save_name = save_name_dir + emoFilenameType + PARAM_FILENAME+".txt" 
				x_scaled = param_range
				if(PARAM_NAME != "FFPType"):
					x_scaled = param_range*motionData[PARAM_NAME].std() + motionData[PARAM_NAME].mean()
				count = 0
				f = open(save_name, 'w')
				for d in y_predicted_param:
					f.write('%lf %lf\n' % (x_scaled[count], d))
					count = count+1
				f.close()

				# return predicted value of the current sample..
				currentprediction_fname = save_name_dir +"currentprediction.txt"
				fpredict = open(currentprediction_fname, 'a+')
				fpredict.write('%lf\n' % y_predicted_original)
				fpredict.close()

				# save range for sliders..
				range_fname = save_name_dir + "range.txt"
				frange = open(range_fname, 'a+')
				frange.write('%lf\n' % min(y_predicted_param))
				frange.write('%lf\n' % max(y_predicted_param))
				frange.close()

				# delete handshake file
				# if handshake.exists():
				# 	os.remove(save_name_dir + "paramsReady.txt")

				#tell cpp that plots are ready..
				save_name2 = save_name_dir + "plotsReady.txt"
				handshake2 = open(save_name2, 'a')
				handshake2.write('%d\n' % PARAM_ID);
				handshake2.close()

				#elapsed = timeit.default_timer() - start_time
				#print(elapsed)

				computeDone = True