import numpy as np
import tensorflow as tf
from tensorflow.contrib import learn
import itertools
import copy
import pandas as pd
import sys

# load dataset
X_train_all_wts = pd.read_csv('C:\\Users\\rutad\\Documents\\bitbucket\\code\\walkingDataset\\trainData.csv', delimiter=',', lineterminator='\n');
X_test_all_wts = pd.read_csv('C:\\Users\\rutad\\Documents\\bitbucket\\code\\walkingDataset\\testData.csv', delimiter=',', lineterminator='\n');
motionData = pd.read_csv('C:\\Users\\rutad\\Documents\\bitbucket\\code\\walkingDataset\\motionData.csv', delimiter=',', lineterminator='\n');

FEATURESALL = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration", "FFPType"]
FEATURES = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration"]
FEATURESALL_WTS = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration", "FFPType", "happier_wt", "sadder_wt", "angrier_wt"]

EMOSCORES = ["happier_mu", "sadder_mu", "angrier_mu"]
EMOCONFIDENCE_INV = ["happier_sigma", "sadder_sigma", "angrier_sigma"]

min_confidence = 25/3 # true skill base confidence: http://trueskill.org/

WTS_LABEL = ["happier_wt", "sadder_wt", "angrier_wt"]
FFP_LABELS = ["walk", "trot", "gallop","dynamic walk", "walk2", "walk2LongSwing", "walkLongSwing"];

#LABEL_ID = 0;
# LABEL_HAPPY = EMOSCORES[0]
# LABEL_SAD = EMOSCORES[1]
WT_COL_HAPPY = WTS_LABEL[0]
WT_COL_SAD = WTS_LABEL[1]

## LOAD TRAINED MODEL..
# def get_input_fn(data_x,data_y, LABEL, num_epochs=None, shuffle=True):
#   return tf.estimator.inputs.pandas_input_fn(
#       x=pd.DataFrame({k: data_x[k].values for k in FEATURESALL_WTS}),
#       y = pd.DataFrame(data_y[LABEL].values),
#       num_epochs=num_epochs,
#       shuffle=shuffle)

# attempt to make tensorflow use multiple cpus..this doesnt seem to do anything..
# with tf.Session(config=tf.ConfigProto(
#    device_count={"CPU":24},
#    inter_op_parallelism_threads=2,
#    intra_op_parallelism_threads=2,
#  )) as sess:

config = tf.ConfigProto(device_count={"CPU": 24},
                        inter_op_parallelism_threads=24,
                        intra_op_parallelism_threads=24, allow_soft_placement=True)

def get_input_fn_predict(data_x,data_y, LABEL, num_epochs=None, shuffle=True):
  return tf.estimator.inputs.pandas_input_fn(
	  x=pd.DataFrame({k: data_x[k].values for k in FEATURESALL_WTS}),
	  num_epochs=num_epochs,
	  shuffle=shuffle)
		  

embedded_ffp_column = tf.contrib.layers.embedding_column(
    tf.contrib.layers.sparse_column_with_integerized_feature("FFPType", 7, combiner=None, dtype=tf.int64), dimension = 7)
# Continuous columns- frontKneesAngle	rearKneesAngle	frontAnkle	rearAnkle	frontHipsAngle
bodyZ = tf.feature_column.numeric_column("bodyAngleZ")
frontK = tf.feature_column.numeric_column("frontKneesAngle")
rearK = tf.feature_column.numeric_column("rearKneesAngle")
frontA = tf.feature_column.numeric_column("frontAnkle")
rearA = tf.feature_column.numeric_column("rearAnkle")
frontH = tf.feature_column.numeric_column("frontHipsAngle")
rearH = tf.feature_column.numeric_column("rearHipsAngle")
swingFtHt = tf.feature_column.numeric_column("swingFtHeight")
speed = tf.feature_column.numeric_column("speed")
gaitTime = tf.feature_column.numeric_column("gaitDuration")

feature_cols = [bodyZ, frontK, rearK, frontA, rearA, frontH, rearH, swingFtHt, speed, gaitTime, embedded_ffp_column]
lin_feature_cols = [bodyZ, swingFtHt, speed, gaitTime]

# Build a wide + deep model (happy)
model_dir_name_happy ="/DNNNew/DNNHappy/wideAndDeep/happy_wideMotionBodyAngDeep_ConfidenceSoftplus2Layer10_10UnitsDropout01EarlyStopLR001"
model_dir_name_sad ="/DNNNew/DNNSad/wideAndDeep/sad_wideMotionBodyAngDeep_ConfidenceELU2Layer1000_1000UnitsDropout04EarlyStopLR001"

regressorHappy = tf.contrib.learn.DNNLinearCombinedRegressor(linear_feature_columns = lin_feature_cols,
  dnn_feature_columns=feature_cols, dnn_hidden_units=[10,10],  model_dir=model_dir_name_happy, weight_column_name = WT_COL_HAPPY,
    dnn_optimizer = tf.train.AdamOptimizer(
      learning_rate=0.001), dnn_dropout =0.1, config=tf.contrib.learn.RunConfig(config), dnn_activation_fn = tf.nn.softplus)
	  
regressorSad = tf.contrib.learn.DNNLinearCombinedRegressor(linear_feature_columns = lin_feature_cols,
  dnn_feature_columns=feature_cols, dnn_hidden_units=[1000,1000],  model_dir=model_dir_name_sad, weight_column_name = WT_COL_SAD,
    dnn_optimizer = tf.train.AdamOptimizer(
      learning_rate=0.001), dnn_dropout =0.4, config=tf.contrib.learn.RunConfig(config), dnn_activation_fn = tf.nn.elu)

# ev1 = regressorHappy.evaluate(
#      input_fn=get_input_fn(X_test_all_wts, y_test, LABEL_HAPPY, num_epochs=1, shuffle=False))
# ev2 = regressorSad.evaluate(
#      input_fn=get_input_fn(X_test_all_wts, y_test, LABEL_SAD, num_epochs=1, shuffle=False))

# temp = regressorHappy.predict_scores(input_fn=get_input_fn_predict(X_train_all_wts, [], [], num_epochs=1, shuffle=False))
# temp2 = regressorSad.predict_scores(input_fn=get_input_fn_predict(X_train_all_wts, [], [], num_epochs=1, shuffle=False))

print("loaded data and NN model")	  