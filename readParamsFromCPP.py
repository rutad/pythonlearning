
save_name_dir =""#"../walkingDataset/tmp"
fname = save_name_dir + "params.txt" 

with open(fname) as f:
    params = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
params =  [x.strip('\n') for x in params] 

print(params)