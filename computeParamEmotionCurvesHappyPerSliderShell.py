## load NN model
from loadPretrainedBestWalkingModel import *

## read params from cpp
save_name_dir ="../walkingDataset/tmp/"
fname = save_name_dir + "params.txt" 
with open(fname) as f:
    params = f.readlines()
params =  [x.strip('\n') for x in params] 


## COMPUTE CURVES
test_feature_vals = pd.DataFrame(index=[0], columns=FEATURESALL)
test_feature_vals = test_feature_vals.fillna(0) # with 0s

PARAM_ID = int(sys.argv[1]) 
print("slide id")
print(PARAM_ID)

PARAM_NAME = FEATURES[PARAM_ID];
param_stepSize = 0.1;
param_range = np.arange(min(X_train_all_wts[PARAM_NAME].min(),X_test_all_wts[PARAM_NAME].min()),
max(X_train_all_wts[PARAM_NAME].max(),X_test_all_wts[PARAM_NAME].max())+param_stepSize,param_stepSize)

count = 1
if(len(params)> len(FEATURESALL)):
	for f in FEATURESALL:
		test_feature_vals[f] = float(params[count])
		count = count + 1

wt_vals = np.array([1,1,1]); # wts
for s in param_range:
	if(s==min(X_train_all_wts[PARAM_NAME].min(),X_test_all_wts[PARAM_NAME].min())): 
		test_vals = np.copy(test_feature_vals.values)
		test_vals = np.append(test_vals, wt_vals)
		test_vals[PARAM_ID] = s;
		X_param_test = test_vals
	else:
		test_vals2 = np.copy(test_feature_vals.values)
		test_vals2 = np.append(test_vals2, wt_vals)
		test_vals2[PARAM_ID] = s;
		X_param_test = np.append(X_param_test, test_vals2,axis=0)
X_param_test = X_param_test.reshape(param_range.size,len(FEATURESALL_WTS))
X_param_test_df = pd.DataFrame(X_param_test, columns = FEATURESALL_WTS)
X_param_test_df["FFPType"]=X_param_test_df["FFPType"].astype(np.int64) 

predictions_param_range = regressor.predict_scores(input_fn=get_input_fn_predict(X_param_test_df, [], LABEL, num_epochs=1, shuffle=False))
y_predicted_param = list(itertools.islice(predictions_param_range,X_param_test_df["FFPType"].size))

PARAM_FILENAME = copy.copy(PARAM_NAME);
if(PARAM_NAME == "bodyAngleZ"):
	PARAM_FILENAME = "body angle";
elif(PARAM_NAME == "swingFtHeight"):
	PARAM_FILENAME = "foot height";
elif(PARAM_NAME == "gaitDuration"):
	PARAM_FILENAME = "gait time";
elif(PARAM_NAME == "frontHipsAngle"):
	PARAM_FILENAME = "front hips";
elif(PARAM_NAME == "rearHipsAngle"):
	PARAM_FILENAME = "rear hips";
elif(PARAM_NAME == "frontKneesAngle"):
	PARAM_FILENAME = "front knees";
elif(PARAM_NAME == "rearKneesAngle"):
	PARAM_FILENAME = "rear knees";
elif(PARAM_NAME == "frontAnkle"):
	PARAM_FILENAME = "front ankles";
elif(PARAM_NAME == "rearAnkle"):
	PARAM_FILENAME = "rear ankles";
save_name = save_name_dir + "/happy_"+ PARAM_FILENAME+"_" +"FFP.txt" 
x_scaled = param_range*motionData[PARAM_NAME].std() + motionData[PARAM_NAME].mean()
count = 0
f = open(save_name, 'w')
for d in y_predicted_param:
	f.write('%lf %lf\n' % (x_scaled[count], d))
	count = count+1
f.close()