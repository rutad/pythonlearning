# import numpy as np
# import tensorflow as tf
# from tensorflow.contrib import learn
# import itertools
# import copy
# import pandas as pd
# import sys

# # load dataset
# X_train_all_wts = pd.read_csv('C:\\Users\\t_desar\\Documents\\bitbucket\\code\\walkingDataset\\trainData.csv', delimiter=',', lineterminator='\n');
# X_test_all_wts = pd.read_csv('C:\\Users\\t_desar\\Documents\\bitbucket\\code\\walkingDataset\\testData.csv', delimiter=',', lineterminator='\n');
# motionData = pd.read_csv('C:\\Users\\t_desar\\Documents\\bitbucket\\code\\walkingDataset\\motionData.csv', delimiter=',', lineterminator='\n');

# FEATURESALL = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration", "FFPType"]
# FEATURES = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration"]
# FEATURESALL_WTS = ["bodyAngleZ", "frontKneesAngle", "rearKneesAngle", "frontAnkle", "rearAnkle", "frontHipsAngle", "rearHipsAngle", "swingFtHeight", "speed", "gaitDuration", "FFPType", "happier_wt", "sadder_wt", "angrier_wt"]

# EMOSCORES = ["happier_mu", "sadder_mu", "angrier_mu"]
# EMOCONFIDENCE_INV = ["happier_sigma", "sadder_sigma", "angrier_sigma"]

# min_confidence = 25/3 # true skill base confidence: http://trueskill.org/

# WTS_LABEL = ["happier_wt", "sadder_wt", "angrier_wt"]
# FFP_LABELS = ["walk", "trot", "gallop","dynamic walk", "walk2", "walk2LongSwing", "walkLongSwing"];

# LABEL_ID = 0;
# LABEL = EMOSCORES[LABEL_ID]
# WT_COL = WTS_LABEL[LABEL_ID]

# ## LOAD TRAINED MODEL..
# def get_input_fn(data_x,data_y, LABEL, num_epochs=None, shuffle=True):
  # return tf.estimator.inputs.pandas_input_fn(
      # x=pd.DataFrame({k: data_x[k].values for k in FEATURESALL_WTS}),
      # y = pd.DataFrame(data_y[LABEL].values),
      # num_epochs=num_epochs,
      # shuffle=shuffle)
	
# def get_input_fn_predict(data_x,data_y, LABEL, num_epochs=None, shuffle=True):
  # return tf.estimator.inputs.pandas_input_fn(
	  # x=pd.DataFrame({k: data_x[k].values for k in FEATURESALL_WTS}),
	  # num_epochs=num_epochs,
	  # shuffle=shuffle)
		  

# embedded_ffp_column = tf.contrib.layers.embedding_column(
    # tf.contrib.layers.sparse_column_with_integerized_feature("FFPType", 7, combiner=None, dtype=tf.int64), dimension = 7)
# # Continuous columns- frontKneesAngle	rearKneesAngle	frontAnkle	rearAnkle	frontHipsAngle
# bodyZ = tf.feature_column.numeric_column("bodyAngleZ")
# frontK = tf.feature_column.numeric_column("frontKneesAngle")
# rearK = tf.feature_column.numeric_column("rearKneesAngle")
# frontA = tf.feature_column.numeric_column("frontAnkle")
# rearA = tf.feature_column.numeric_column("rearAnkle")
# frontH = tf.feature_column.numeric_column("frontHipsAngle")
# rearH = tf.feature_column.numeric_column("rearHipsAngle")
# swingFtHt = tf.feature_column.numeric_column("swingFtHeight")
# speed = tf.feature_column.numeric_column("speed")
# gaitTime = tf.feature_column.numeric_column("gaitDuration")

# feature_cols = [bodyZ, frontK, rearK, frontA, rearA, frontH, rearH, swingFtHt, speed, gaitTime, embedded_ffp_column]
# lin_feature_cols = [bodyZ, swingFtHt, speed, gaitTime]

# # Build a wide + deep model
# model_dir_name="/DNN/wideAndDeep/wideMotionBodyAngDeepAll_ConfidenceSoftplus2Layer10_10UnitsDropout01EarlyStopLR001"

# regressor = tf.contrib.learn.DNNLinearCombinedRegressor(linear_feature_columns = lin_feature_cols,
  # dnn_feature_columns=feature_cols, dnn_hidden_units=[10,10],  model_dir=model_dir_name, weight_column_name = WT_COL,
    # dnn_optimizer = tf.train.AdamOptimizer(
      # learning_rate=0.001), dnn_dropout =0.1, config=tf.contrib.learn.RunConfig(save_checkpoints_secs=1), dnn_activation_fn = tf.nn.softplus)

	 
from loadPretrainedBestWalkingModel import *
import timeit
from fast_predict import *
	 
## COMPUTE CURVES
test_feature_vals = pd.DataFrame(index=[0], columns=FEATURESALL)
test_feature_vals = test_feature_vals.fillna(0) # with 0s

save_name_dir ="../walkingDataset/tmp"

PARAM_ID = 0 #int(sys.argv[1])
PARAM_NAME = FEATURES[PARAM_ID];
param_stepSize = 0.1;
param_range = np.arange(min(X_train_all_wts[PARAM_NAME].min(),X_test_all_wts[PARAM_NAME].min()),
max(X_train_all_wts[PARAM_NAME].max(),X_test_all_wts[PARAM_NAME].max())+param_stepSize,param_stepSize)

count = 2
#if(len(sys.argv)> len(FEATURESALL)):
for f in FEATURESALL:
	test_feature_vals[f] = 0.0#float(sys.argv[count])
	#count = count + 1

wt_vals = np.array([1,1,1]); # wts
for s in param_range:
	if(s==min(X_train_all_wts[PARAM_NAME].min(),X_test_all_wts[PARAM_NAME].min())): 
		test_vals = np.copy(test_feature_vals.values)
		test_vals = np.append(test_vals, wt_vals)
		test_vals[PARAM_ID] = s;
		X_param_test = test_vals
	else:
		test_vals2 = np.copy(test_feature_vals.values)
		test_vals2 = np.append(test_vals2, wt_vals)
		test_vals2[PARAM_ID] = s;
		X_param_test = np.append(X_param_test, test_vals2,axis=0)
X_param_test = X_param_test.reshape(param_range.size,len(FEATURESALL_WTS))
X_param_test_df = pd.DataFrame(X_param_test, columns = FEATURESALL_WTS)
X_param_test_df["FFPType"]=X_param_test_df["FFPType"].astype(np.int64) 

start_time = timeit.default_timer()
#predictions_param_range = FastPredict(regressorHappy.predict_scores(input_fn=get_input_fn_predict(X_param_test_df, [], [], num_epochs=1, shuffle=False)))
predictions_param_range = regressorHappy.predict_scores(input_fn=get_input_fn_predict(X_param_test_df, [], [], num_epochs=1, shuffle=False))
y_predicted_param = list(itertools.islice(predictions_param_range,X_param_test_df["FFPType"].size))
elapsed = timeit.default_timer() - start_time
print(elapsed)

# PARAM_FILENAME = copy.copy(PARAM_NAME);
# if(PARAM_NAME == "bodyAngleZ"):
# 	PARAM_FILENAME = "body angle";
# elif(PARAM_NAME == "swingFtHeight"):
# 	PARAM_FILENAME = "foot height";
# elif(PARAM_NAME == "gaitDuration"):
# 	PARAM_FILENAME = "gait time";
# elif(PARAM_NAME == "frontHipsAngle"):
# 	PARAM_FILENAME = "front hips";
# elif(PARAM_NAME == "rearHipsAngle"):
# 	PARAM_FILENAME = "rear hips";
# elif(PARAM_NAME == "frontKneesAngle"):
# 	PARAM_FILENAME = "front knees";
# elif(PARAM_NAME == "rearKneesAngle"):
# 	PARAM_FILENAME = "rear knees";
# elif(PARAM_NAME == "frontAnkle"):
# 	PARAM_FILENAME = "front ankles";
# elif(PARAM_NAME == "rearAnkle"):
# 	PARAM_FILENAME = "rear ankles";
# save_name = save_name_dir + "/happy_"+ PARAM_FILENAME+"_" +"FFP.txt"
# x_scaled = param_range*motionData[PARAM_NAME].std() + motionData[PARAM_NAME].mean()
# count = 0
# f = open(save_name, 'w')
# for d in y_predicted_param:
# 	f.write('%lf %lf\n' % (x_scaled[count], d))
# 	count = count+1
# f.close()