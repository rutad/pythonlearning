import numpy as np
import itertools
import copy
import pandas as pd
import sys
from sklearn.externals import joblib
from sklearn.linear_model import LinearRegression
import statsmodels.formula.api as smf
import statsmodels.api as sm
import statsmodels

FEATURES = ["WtCohesion", "WtAlignment", "WtSeparation", "WtSeek", "wtExpBlend", "Speed", "Neighbourhood", "WanderFreq", "wanderRadius", "kickTime"] 
FEATURESALL = ["WtCohesion", "WtAlignment", "WtSeparation", "WtSeek", "wtExpBlend", "Speed", "Neighbourhood", "WanderFreq", "wanderRadius", "kickTime","VelDirType","dampExplore"]
FEATURES_FFP = ["WtCohesion", "WtAlignment", "WtSeparation", "WtSeek", "wtExpBlend", "Speed", "Neighbourhood", "WanderFreq", "wanderRadius", "kickTime", "forward", "backward", "right side","left side", "upwards", "downwards", "exploreOff", "exploreOn"]
FFP_LABELS = ["forward", "backward", "right side","left side", "upwards", "downwards", "exploreOff", "exploreOn"]

EMOSCORES = ["happier_mu", "sadder_mu", "angrier_mu", "more-afraid_mu", "more-surprised_mu", "more-disgusted_mu"]
EMOCONFIDENCE_INV = ["happier_sigma", "sadder_sigma", "angrier_sigma", "more-afraid_sigma","more-surprised_sigma", "more-disgusted_sigma"]
min_confidence = 25/3 # true skill base confidence: http://trueskill.org/
WTS_LABEL = ["happier_wt", "sadder_wt", "angrier_wt", "more-afraid_wt","more-surprised_wt", "more-disgusted_wt"]

model_dir_name = "/Users/rutad/Documents/bitbucket/pythonTF/LRBaselineArm/"

# load dataset
X_train_all_wts = pd.read_csv(model_dir_name+"XTrainLR.csv", delimiter=',', lineterminator='\n');
X_test_all_wts = pd.read_csv(model_dir_name+"XTestLR.csv", delimiter=',', lineterminator='\n');
motionData = pd.read_csv('C:\\Users\\rutad\\Documents\\bitbucket\\pythonTF\\armMotionDataset.csv', delimiter=',', lineterminator='\n');
motionData.rename(columns={"motionTime\r": "motionTime"},inplace=True)

regressor_happy = joblib.load(model_dir_name + "SLRTrainedArm_"+EMOSCORES[0]+".pkl")
regressor_sad = joblib.load(model_dir_name + "SLRTrainedArm_"+EMOSCORES[1]+".pkl")
regressor_angry = joblib.load(model_dir_name + "SLRTrainedArm_"+EMOSCORES[2]+".pkl")
regressor_scared = joblib.load(model_dir_name + "SLRTrainedArm_"+EMOSCORES[3]+".pkl")
regressor_surprised = joblib.load(model_dir_name + "SLRTrainedArm_"+EMOSCORES[4]+".pkl")
regressor_disgusted = joblib.load(model_dir_name + "SLRTrainedArm_"+EMOSCORES[5]+".pkl")

#print("DONE!")