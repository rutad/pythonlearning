## load NN model
from pathlib import Path
import os
import timeit
from loadPretrainedSLRWalkingModel import *

save_name_dir ="../walkingDataset/tmp/"
handshake = Path(save_name_dir + "paramsReady.txt");

while True:
	if handshake.exists():

		## read params from cpp
		fname = save_name_dir + "params.txt" 
		with open(fname) as f:
		    params = f.readlines()
		params =  [x.strip('\n') for x in params] 

		## preprocess params
		test_feature_vals = pd.DataFrame(index=[0], columns=FEATURES_FFP)
		test_feature_vals = test_feature_vals.fillna(0) # with 0s
		count = 0
		if(len(params)== len(FEATURESALL) + 1):
			for f in FEATURESALL:
				if(f!="FFPType"):
					test_feature_vals[f] = (float(params[count]) - motionData[f].mean())/motionData[f].std();
				else:
					for j in range(0,7):
						if(abs(float(params[count]) - j)<0.001):
							test_feature_vals[FFP_LABELS[j]] = 1.0
				count = count + 1
		#actual sample
		X_param_original_df = test_feature_vals.copy()

		## COMPUTE CURVES For all params 
		for id in range(0,11):
			PARAM_ID = id
			PARAM_NAME = FEATURESALL[PARAM_ID];
			#print(PARAM_ID)
			#print(PARAM_NAME)

			# create a range for param we are varying..
			num_eval = 5;
			param_stepSize = 1;
			param_range = []
			if(PARAM_NAME == "FFPType"):
				param_range = np.array([0,1,2,3,4,5,6]);
			else:
				param_stepSize = (max(X_train_all_wts[PARAM_NAME].max(),X_test_all_wts[PARAM_NAME].max()) - min(X_train_all_wts[PARAM_NAME].min(),X_test_all_wts[PARAM_NAME].min()))/float(num_eval);
				param_range = np.arange(min(X_train_all_wts[PARAM_NAME].min(),X_test_all_wts[PARAM_NAME].min()),
					max(X_train_all_wts[PARAM_NAME].max(),X_test_all_wts[PARAM_NAME].max()) + param_stepSize,param_stepSize)
				param_range = np.append(param_range, test_feature_vals[PARAM_NAME])
				param_range = np.sort(param_range)

			# for the range, create a stack(dataframe) of all the points on which we will predict at once..
			for s in param_range:
				if(PARAM_NAME!="FFPType"):
					if(s==min(param_range)): 
						test_vals = test_feature_vals.copy()
						test_vals[PARAM_NAME] = s;
						X_param_test = test_vals
					else:
						test_vals2 = test_feature_vals.copy()
						test_vals2[PARAM_NAME] = s;
						X_param_test = pd.concat([X_param_test, test_vals2],axis=0)
				else:
					if(s==0): 
						test_vals = test_feature_vals.copy()
						for j in range(0,7):
							if(test_vals.iloc[0][FFP_LABELS[j]]>0.0):
								test_vals.at[0,FFP_LABELS[j]] =0.0;
							if(abs(s - j)<0.001):
								test_vals[FFP_LABELS[j]] = 1.0
						X_param_test = test_vals
					else:
						test_vals2 = test_feature_vals.copy()
						for j in range(0,7):
							if(test_vals2.iloc[0][FFP_LABELS[j]]>0):
								test_vals2.at[0,FFP_LABELS[j]] =0.0;
							if(abs(s - j)<0.001):
								test_vals2[FFP_LABELS[j]] = 1.0
						X_param_test = pd.concat([X_param_test, test_vals2],axis=0)

			X_param_test_concat_df = pd.concat([X_param_test, X_param_original_df])
			X_param_test_concat_df = statsmodels.tools.add_constant(X_param_test_concat_df,has_constant='add')
			X_original_crosscheck = statsmodels.tools.add_constant(X_param_original_df,has_constant='add')
			#print(X_original_crosscheck)

			start_time = timeit.default_timer()

			# choose different regressor options based on emotion type..
			if(int(params[11]) == 0):
				predictions_param_range = regressor_happy.get_prediction(X_param_test_concat_df).summary_frame(alpha=0.05)
				# predict_check = np.dot(X_original_crosscheck.values,happy_model.reshape(18,1))
				emoFilenameType = "happy_"
			elif(int(params[11]) == 1):
				predictions_param_range = regressor_sad.get_prediction(X_param_test_concat_df).summary_frame(alpha=0.05)
				# predict_check = np.dot(X_original_crosscheck.values,sad_model.reshape(18,1))
				emoFilenameType = "sad_"
			elif(int(params[11]) == 2):
				predictions_param_range = regressor_angry.get_prediction(X_param_test_concat_df).summary_frame(alpha=0.05)
				# predict_check = np.dot(X_original_crosscheck.values,angry_model.reshape(18,1))
				emoFilenameType = "angry_"
			elif(int(params[11]) == 3):
				predictions_param_range = regressor_scared.get_prediction(X_param_test_concat_df).summary_frame(alpha=0.05)
				# predict_check = np.dot(X_original_crosscheck.values,scared_model.reshape(18,1))
				emoFilenameType = "scared_"
			elif(int(params[11]) == 5):
				predictions_param_range = regressor_surprised.get_prediction(X_param_test_concat_df).summary_frame(alpha=0.05)
				# predict_check = np.dot(X_original_crosscheck.values,scared_model.reshape(18,1))
				emoFilenameType = "surprised_"
			elif(int(params[11]) == 4):
				predictions_param_range = regressor_disgusted.get_prediction(X_param_test_concat_df).summary_frame(alpha=0.05)
				# predict_check = np.dot(X_original_crosscheck.values,scared_model.reshape(18,1))
				emoFilenameType = "disgusted_"


			y_predicted_param_all = predictions_param_range["mean"].tolist()
			num_pred = len(y_predicted_param_all)
			y_predicted_param = y_predicted_param_all[:num_pred-1]
			y_predicted_original = y_predicted_param_all[-1]

			## crosschecking for gradient computation
			#print(y_predicted_original)
			# print(predict_check)
			# print(predict_check - y_predicted_original)
			# input("press enter")

			predictions_param_range.index = range(0,num_pred)

			if(y_predicted_original< min(y_predicted_param) or y_predicted_original> max(y_predicted_param)):
				print("SOMETHING IS WRONG!")
				input("press enter")


			elapsed = timeit.default_timer() - start_time
			#print(elapsed)
			#input("Press Enter to continue...")

			# save plot data in file..
			PARAM_FILENAME = copy.copy(PARAM_NAME);
			if(PARAM_NAME == "bodyAngleZ"):
				PARAM_FILENAME = "body angle";
			elif(PARAM_NAME == "swingFtHeight"):
				PARAM_FILENAME = "foot height";
			elif(PARAM_NAME == "gaitDuration"):
				PARAM_FILENAME = "gait time";
			elif(PARAM_NAME == "frontHipsAngle"):
				PARAM_FILENAME = "front hips";
			elif(PARAM_NAME == "rearHipsAngle"):
				PARAM_FILENAME = "rear hips";
			elif(PARAM_NAME == "frontKneesAngle"):
				PARAM_FILENAME = "front knees";
			elif(PARAM_NAME == "rearKneesAngle"):
				PARAM_FILENAME = "rear knees";
			elif(PARAM_NAME == "frontAnkle"):
				PARAM_FILENAME = "front ankles";
			elif(PARAM_NAME == "rearAnkle"):
				PARAM_FILENAME = "rear ankles";
			elif(PARAM_NAME == "FFPType"):
				PARAM_FILENAME = "gait pattern";
			save_name = save_name_dir + emoFilenameType + PARAM_FILENAME+".txt" 
			x_scaled = param_range
			if(PARAM_NAME != "FFPType"):
				x_scaled = param_range*motionData[PARAM_NAME].std() + motionData[PARAM_NAME].mean()
			count = 0
			f = open(save_name, 'w')
			for d in y_predicted_param:
				f.write('%lf %lf %lf %lf\n' % (x_scaled[count], d,  predictions_param_range.at[count, "mean_ci_lower"], predictions_param_range.at[count, "mean_ci_upper"]))
				count = count+1
			f.close()

			# return predicted value of the current sample..
			currentprediction_fname = save_name_dir +"currentprediction.txt"
			fpredict = open(currentprediction_fname, 'a+')
			fpredict.write('%lf\n' % y_predicted_original)
			fpredict.close()

			# save range for sliders..
			range_fname = save_name_dir + "range.txt"
			frange = open(range_fname, 'a+')
			frange.write('%lf\n' % min(y_predicted_param_all))
			frange.write('%lf\n' % max(y_predicted_param_all))
			frange.close()

			#tell cpp that plots are ready..
			print(PARAM_ID)
			save_name2 = save_name_dir + "plotsReady.txt"
			handshake2 = open(save_name2, 'a+')
			handshake2.write('%d\n' % PARAM_ID);
			handshake2.close()

		# delete handshake file
		if handshake.exists():
			os.remove(save_name_dir + "paramsReady.txt")
			print("run end")
